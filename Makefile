jpg = $(patsubst %.flame,%.jpg,$(wildcard *.flame))
png = $(patsubst %.flame,%.png,$(wildcard *.flame))
xcf = $(patsubst %.flame,%.xcf,$(wildcard *.flame))

all: jpg

png: $(png)

xcf: $(xcf)

jpg: $(jpg)

%.png: %.flame
	nice -n 5 env out=$@ format=png use_mem=1024M nstrips=1 $(FLAM3OPTS) flam3-render < $<

%.xcf: %.png
	echo '(make-fractal-border "$<" "$@" "scripts/ianweller.png") (gimp-quit 0)' | cat scripts/fractal-border.scm - | gimp -i -b -

%.jpg: %.xcf
	echo '(make-fractal-jpeg "$<" "$@") (gimp-quit 0)' | cat scripts/jpeg.scm - | gimp -i -b -

clean:
	rm -f *.jpg *.png *.xcf

.PHONY: all png xcf jpg clean
