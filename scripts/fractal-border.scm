(define (make-fractal-border infile outfile logofile)
  (let*
    (
      (image (car (gimp-file-load 1 infile infile)))
      (fractal (car (gimp-image-get-active-layer image)))
      (fwidth (car (gimp-drawable-width fractal)))
      (fheight (car (gimp-drawable-height fractal)))
      (logo (car (gimp-file-load-layer 1 image logofile)))
      (lwidth (car (gimp-drawable-width logo)))
      (lheight (car (gimp-drawable-height logo)))
      (innerborder (car (gimp-layer-new image (+ 30 fwidth) (+ 30 fheight) 1 "Inner Border" 100 0)))
      (whiteborder (car (gimp-layer-new image (+ 240 fwidth) (+ 240 fheight) 1 "White Border" 100 0)))
      (outerborder (car (gimp-layer-new image (+ 300 fwidth) (+ 300 fheight) 1 "Outer Border" 100 0)))
      (black '(0 0 0))
      (white '(248 248 248))
    )
    ; set layer name
    (gimp-layer-set-name fractal "Fractal")
    ; resize canvas
    (gimp-image-resize image (+ 300 fwidth) (+ 300 fheight) 150 150)
    ; new layer: inner border
    (gimp-layer-set-offsets innerborder 135 135)
    (gimp-context-set-foreground black)
    (gimp-drawable-fill innerborder 0)
    (gimp-image-add-layer image innerborder 1)
    ; new layer: white border
    (gimp-layer-set-offsets whiteborder 30 30)
    (gimp-context-set-foreground white)
    (gimp-drawable-fill whiteborder 0)
    (gimp-image-add-layer image whiteborder 2)
    ; new layer: outer border
    (gimp-layer-set-offsets outerborder 0 0)
    (gimp-context-set-foreground black)
    (gimp-drawable-fill outerborder 0)
    (gimp-image-add-layer image outerborder 3)
    ; new layer: logo
    (gimp-layer-set-opacity logo 42)
    (gimp-layer-set-offsets logo (- (- (car(gimp-image-width image)) lwidth) 45) (- (- (car(gimp-image-height image)) lheight) 45))
    (gimp-image-add-layer image logo 2)
    ; save
    (gimp-xcf-save 0 image fractal outfile outfile)
  )
)
