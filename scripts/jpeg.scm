(define (make-fractal-jpeg infile outfile)
  (let*
    (
      (image (car (gimp-file-load 1 infile infile)))
      (drawable (car (gimp-image-merge-visible-layers image 2)))
    )
    (file-jpeg-save 1 image drawable outfile outfile 0.97 0 1 0 "" 3 1 0 1)
  )
)
